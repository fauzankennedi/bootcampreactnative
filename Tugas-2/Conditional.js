console.log ("Tugas Conditional")
console.log ("Tugas If-else")
var nama = ""
var peran = ""
{if (nama == '' || peran == ''){
    console.log("Nama harus diisi!!")
}
else if (nama == 'John' || peran == ''){
    console.log("Halo Kenn, pilih peranmu untuk memulai game!")
}
else if (nama == 'Jean' || peran == 'Penyihir'){
    console.log("Halo Penyihir Kenn, kamu dapat melihat siapa yang menjadi werewolf!")
}
else if (nama == 'jeanita' || peran == 'Guard'){
    console.log("Halo Guard Kenn, kamu dapat membantu melindungi temanmu dari serangan werewolf")
}
else if (nama == 'Junaedi' || peran == "Werewolf"){
    console.log("Halo Werewolf Kenn, Kamu akan memakan mangsa setiap malam!")
}}

console.log("=============")
console.log ("Tugas Switch Case")
var hari = 1
var bulan = 5
var tahun = 1945

switch(bulan) {
    case 1: bulan = ("Januari"); break;
    case 2: bulan = ("Februari"); break;
    case 3: bulan = ("Maret"); break;
    case 4: bulan = ("April"); break;
    case 5: bulan = ("Mei"); break;
    case 6: bulan = ("Juni"); break;
    case 7: bulan = ("Juli"); break;
    case 8: bulan = ("Agustus"); break;
    case 9: bulan = ("September"); break;
    case 10: bulan = ("Oktober"); break;
    case 11: bulan = ("November"); break;
    case 12: bulan = ("Desember"); break;
    default: bulan = (''); 
}

var tanggal = (hari + ' ' + bulan + ' ' + tahun)
console.log(tanggal)
console.log("Output Soal 1")
console.log("Output Pertama")
var deret = 0
var kelipatan = 2
while(deret < 20){
    deret += kelipatan;
    console.log(deret + ' - I love coding');
}
console.log("Output Kedua")
var deret = 22
var kelipatan = 2
while(deret > 2){
    deret -= kelipatan;
    console.log(deret + ' - I will become a mobile developer');
}
console.log("==========")
console.log("Output Soal 2")
for(var deret2 = 1; deret2 < 21; deret2++){
    if(deret2 % 2 == 1){
        if(deret2 % 3 == 0){
            console.log(deret2 + " = I love coding")
        }
        else{
            console.log(deret2 + " = Santai")
        }
    }
    else if(deret2 % 2 == 0)
        console.log(deret2 + " = Berkualitas")
}
console.log("==========")
console.log("Output Soal 3")
for(var pagar = 0; pagar < 4; pagar += 1){
    console.log("#" + "#" + "#" + "#" + "#" + "#" + "#" + "#")
}

console.log("==========")
console.log("Output Soal 4")
var hasil = '';
    for (var i = 1; i < 8; i++) {
        for (var j = 1; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    console.log(hasil);
console.log("==========")
console.log("Output Soal 5")
for(var papan = 0; papan < 8; papan++){
    if(papan % 2==0){
        console.log(" # # # #")
    }
    else if(papan % 2!=0)
        console.log("# # # # ")
}
console.log("Output Soal 1")
function range(startNum, finishNum) {
    var arr = [];

    if(startNum > finishNum){
        var rangelength = startNum - finishNum + 1;
        for (var i = 0; i < rangelength; i++){
            arr.push(startNum - i);
        }
    }
    else if(startNum < finishNum){
        var rangelength = finishNum - startNum - 1;
        for (var i = 0; i < rangelength; i++){
            arr.push(startNum + i);
        }
    }
    else if(!startNum || !finishNum){
        return -1;
    }
    return arr
}
console.log(range(1, 10)) 
console.log(range(1)) 
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range()) 


console.log("==========")
console.log("Output Soal 2")
function rangeWithStep(startNum, finishNum, step) {
    var arr2 = [];

    if(startNum > finishNum){
        var currentnum = startNum;
        for (var i = 0; currentnum >= finishNum; i++){
            arr2.push(currentnum);
            currentnum -= step;
        }
    }
    else if(startNum < finishNum){
        var currentnum = startNum;
        for (var i = 0; currentnum <= finishNum; i++){
            arr2.push(currentnum);
            currentnum += step;
        }
    }
    else if(!startNum || !finishNum || !step){
        return -1;
    }
    return arr2
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

console.log("==========")
console.log("Output Soal 3")
function sum(startNum, finishNum, step){
    var arr3 = [];
    var distance = [];

    if(!step){
        distance = 1
    }
    else{
        distance = step
    }

    if(startNum > finishNum){
        var currentnum = startNum;
        for (var i = 0; currentnum >= finishNum; i++){
            arr3.push(currentnum);
            currentnum -= distance;
        }
    }
    else if(startNum < finishNum){
        var currentnum = startNum;
        for (var i = 0; currentnum <= finishNum; i++){
            arr3.push(currentnum);
            currentnum += distance;
        }
    }
    else if(!startNum && !finishNum && !step){
        return 0;
    }
    else if(startNum){
        return startNum;
    }
    var total = 0;
        for (var i = 0; i < arr3.length; i++){
        total = total + arr3[i]
    }
    return total
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2)) 
console.log(sum(1))
console.log(sum())

console.log("==========")
console.log("Output Soal 4")
function dataHandling (data){
    var dataLength = data.length;

    for (var i = 0; i < dataLength; i++){
        var nomorID = "Nomor ID : " + data[i][0];
        var nama = "Nama : " + data[i][1];
        var TTL = "TTL : " + data[i][2] + " " + data[i][3];
        var hobi = "Hobi : " + data[i][4];

        console.log(nomorID)
        console.log(nama)
        console.log(TTL)
        console.log(hobi)
    }

}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

console.log("==========")
console.log("Output Soal 5")
function balikKata (kata){
    var kataBaru = " ";

    for(var i = kata.length-1; i >= 0; i--){
        kataBaru += kata[i]
    }
    return kataBaru
}

console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 


console.log("==========")
console.log("Output Soal 6")
function dataHandling2 (data2){
    var newData = data2;
    var newName = data2[1] + "Elsharawy"
    var newProvince = "Provinsi" + data2[2]
    var gender = "Pria"
    var institusi = "SMA International Metro"

    newData.slice(1, 1, newData)
    newName.slice(2, 1, newProvince)
    newProvince.slice(4, 1, gender, institusi)

    var arrDate = data2[3]
    var newDate = arrDate.split('/')
    var monthNum = newDate[1]
    var monthname = ""

    switch(monthNum){
        case "01" :
        monthname = "Januari";
            break;
        case "02" :
        monthname = "Februari";
            break;
        case "03" :
        monthname = "Maret";
            break;
        case "04" :
        monthname = "April";
            break;
        case "05" :
        monthname = "Mei";
            break;
        case "06" :
        monthname = "Juni";
            break;
        case "07" :
        monthname = "Juli";
            break;
        case "08" :
        monthname = "Agustus";
            break;  
        case "09" :
        monthname = "September";
            break;
        case "10" :
        monthname = "Oktober";
            break;
        case "11" :
        monthname = "Noevember";
            break;
        case "12" :
        monthname = "Desember";
            break;
        default :
            break;
    }
    var dateJoin = newData.join("-")
    var dateArr = newDate.sort(function(value1,value2) {
        value2 - value1
    })
    var editName = newName.slice(0,15)
    console.log(newData)
    console.log(monthname)
    console.log(dateJoin)
    console.log(editName)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]

dataHandling2(input)
console.log("Output Soal 1")
function arrayToObject() {
        var person1 = { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        birthYear: 1976
        }
        var person2 = { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female",
        birthYear: "Invalid Birth Year"
        }
        
    var now = new Date(2021);
    var thisYear = now.getFullYear();


    console.log(1 + ". " + "First name: " + person1.firstName  + "\n Last name: " + person1.lastName + "\n Gender: " + person1.gender + "\n Age: " + (now - person1.birthYear))
    console.log(2 + ". " + "First name: " + person2.firstName  + "\n Last name: " + person2.lastName + "\n Gender: " + person2.gender + "\n Age: " + (now - person2.birthYear))    
}
arrayToObject([])

"\n"
console.log("==========")
console.log("Output Soal 2")
function shoppingTime(memberId, money) {
    var listPurchased1 = {
        "Sepatu Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju H&N": 250000,
        "Sweater Uniklooh": 175000,
        "Casing Handphone": 50000
    }

    if (!memberId){
        return("Mohon maaf, toko X hanya berlaku untuk member saja")
    }
    else if (money <= 50000){
        return("Mohon maaf, uang tidak cukup")
    }
    else {
        var newObject = {};
        var changeMoney = money;
        var listPurchased = [];
        var sepatuStacattu = "Sepatu Stacattu";
        var bajuZoro = "Baju Zoro";
        var bajuHN = "Baju H&N";
        var sweaterUniklooh = "Sweater Uniklooh";
        var casingHandphone = "Casing Handphone";

        var check = 0;
        for (var i = 0; changeMoney >= 50000 && check ==0; i++){
            if (changeMoney >= 1500000){
                listPurchased.push(sepatuStacattu)
                changeMoney -= 1500000
            }
            else if (changeMoney >= 500000){
                listPurchased.push(bajuZoro)
                changeMoney -= 500000
            }
            else if (changeMoney >= 250000){
                listPurchased.push(bajuHN)
                changeMoney -= 250000
            }
            else if (changeMoney >= 175000){
                listPurchased.push(sweaterUniklooh)
                changeMoney -= 175000
            }
            else if (changeMoney >= 50000){
                for (var j = 0; j <= listPurchased.length-1; j++){
                    if (listPurchased[j] == casingHandphone)
                        check += 1
                    }
                    if (check == 0){
                        listPurchased.push(casingHandphone)
                        changeMoney -= 50000
                    }
                    else {
                            listPurchased.push(casingHandphone)
                            changeMoney -= 50000
                    }
        }
    }
    newObject.memberId = memberId;
    newObject.changeMoney = changeMoney;
    newObject.listPurchased = listPurchased;
    newObject.changeMoney = changeMoney;
    return newObject
}}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("==========")
console.log("Output Soal 3")
function naikAngkot(arrPenumpang, naikDari, tujuan) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var name = arrPenumpang;
    var dari = naikDari;
    var sampe = tujuan;
    var ongkos = {
        1: 2000,
        2: 4000,
        3: 6000,
        4: 8000
    }
    return("penumpang: " + name + ", naikDari: " + dari + ", tujuan: " + sampe + ", bayar: " + ongkos[1])
}
   
  //TEST CASE
  console.log(naikAngkot('Dimitri', 'B', 'F'));
  console.log(naikAngkot('Icha', 'A', 'B'));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]


console.log("Output Soal 1")
console.log("Normal Javasrcipt : ")
var golden = function goldenFunction(){
    console.log("this is golden!!")
}
   
  golden()
console.log("ES6 : ")
var newgolden = appFunction = () => {
    console.log("this is golden!!")
} 

newgolden();

console.log("==========")
console.log("Output Soal 2")
console.log("Normal Javasrcipt : ")
const Function = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  Function("William", "Imoh").fullName() 

  console.log("ES6 : ")
  const newFunction = literal = (firstName, lastName) => {
    return {fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

console.log("==========")
console.log("Output Soal 3")
console.log("Normal Javasrcipt : ")
const Object = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const firstName = Object.firstName;
const lastName = Object.lastName;
const destination = Object.destination;
const occupation = Object.occupation;
// Driver code
console.log(firstName, lastName, destination, occupation)

console.log("ES6 : ")
const newObject = {
    firstName1: "Harry",
    lastName1: "Potter Holt",
    destination1: "Hogwarts React Conf",
    occupation1: "Deve-wizard Avocado",
    spell1: "Vimulus Renderus!!!"
  }
const {firstName1, lastName1, destination1, occupation1, spell1} = newObject
console.log(firstName1, lastName1, destination1, occupation1)

console.log("==========")
console.log("Output Soal 4")
console.log("Normal Javasrcipt : ")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)

console.log("ES6 : ")
const newWest = ["Will", "Chris", "Sam", "Holly"]
const newEast = ["Gill", "Brian", "Noel", "Maggie"]
let combinedArray = [newWest, newEast]

console.log(combined)


console.log("==========")
console.log("Output Soal 5")
console.log("Normal Javasrcipt : ")
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before)

console.log("ES6 : ")
const newPlanet = "earth"
const newView = "glass"
var newBefore = `Lorem ${newView}dolor sit amet, consectetur adipiscing elit,${newPlanet}do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(newBefore)